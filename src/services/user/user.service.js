import { ApiPath, ContentType, HttpMethod } from 'common/enums/enums';

class User {
  constructor({ baseUrl, http }) {
    this._baseUrl = baseUrl;
    this._http = http;
    this._basePath = ApiPath.USERS;
  }

  getAll() {
    return this._http.load(`${this._baseUrl}${this._basePath}`, {
      method: HttpMethod.GET,
    });
  }

  getOne(user) {
    return this._http.load(`${this._baseUrl}${this._basePath}?username=${user.username}&password=${user.password}`, {
      method: HttpMethod.GET,
    });
  }

  deleteUser(id) {
    return this._http.load(`${this._baseUrl}${this._basePath}/${id}`, {
      method: HttpMethod.DELETE,
    });
  }

  add(user) {
    return this._http.load(`${this._baseUrl}${this._basePath}`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(user),
    });
  }
  
  edit(id, payload) {
    return this._http.load(`${this._baseUrl}${this._basePath}/${id}`, {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }
}

export { User };
