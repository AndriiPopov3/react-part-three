import { ApiPath, ContentType, HttpMethod } from 'common/enums/enums';

class Message {
  constructor({ baseUrl, http }) {
    this._baseUrl = baseUrl;
    this._http = http;
    this._basePath = ApiPath.MESSAGES;
  }

  getAll() {
    return this._http.load(`${this._baseUrl}${this._basePath}`, {
      method: HttpMethod.GET,
    });
  }

  add(message) {
      return this._http.load(`${this._baseUrl}${this._basePath}`, {
        method: HttpMethod.POST,
        contentType: ContentType.JSON,
        payload: JSON.stringify(message),
      });
  }

  deleteMessage(id) {
    return this._http.load(`${this._baseUrl}${this._basePath}/${id}`, {
      method: HttpMethod.DELETE,
    });
  }

  edit(id, payload) {
    return this._http.load(`${this._baseUrl}${this._basePath}/${id}`, {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }
}

export { Message };
