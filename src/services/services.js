import { ENV } from 'common/enums/enums';
import { Http } from './http/http.service';
import { User } from './user/user.service';
import { Message } from './message/message.service';

const http = new Http();
const user = new User({
  baseUrl: ENV.API.URL,
  http,
});
const message = new Message({
  baseUrl: ENV.API.URL,
  http,
});

export { http, user, message };
