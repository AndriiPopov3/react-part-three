import { AppPath } from 'common/enums/enums';
import { Switch, Route } from 'components/common/common';
import Chat from 'components/chat/Chat';
import Login from 'components/login/login';
import EditMessage from 'components/chat/components/editMessage/editMessage';
import UserList from 'components/userList/userList';
import EditUser from 'components/editUser/editUser';

const App = () => (
  <>
    <main>
      <Switch>
        <Route path={AppPath.CHAT} exact component={Chat} />
        <Route path={AppPath.ROOT} exact component={Login} />
        <Route path={AppPath.EDIT_MESSAGE} exact component={EditMessage} />
        <Route path={AppPath.USER_LIST} exact component={UserList} />
        <Route path={AppPath.EDIT_USER} exact component={EditUser} />
        <Route path={AppPath.ANY} exact component={Login}  />
      </Switch>
    </main>
  </>
);

export default App;
