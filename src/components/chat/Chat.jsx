import { v4 as uuidv4 } from 'uuid';
import toast, { Toaster } from 'react-hot-toast';
import { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppPath } from 'common/enums/enums';
import { chat as chatActionCreator } from 'store/actions';
import { Redirect } from 'react-router';
import { Link } from 'components/common/common';
import Preloader from './components/preloader/Preloader';
import Header from './components/header/Header';
import MessageList from './components/messageList/MessageList';
import MessageInput from './components/messageInput/MessageInput';
import './chat.css';

const Chat = () => {
    const { messages, status, preloader, error, currentUser } = useSelector(({ chat, login }) => ({
        messages: chat.messages,
        status: chat.status,
        preloader: chat.preloader,
        error: chat.error,
        currentUser: login.user
      }));
    const [newMessage, setNewMessage] = useState("");
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(chatActionCreator.loadMessages());
    }, [dispatch]);
    
    const countUsers = (messages) => {
        let res = 1;
        for (let i = 1; i < messages.length; i++) {
            let j = 0;
            for (j = 0; j < i; j++)
                if (messages[i].user === messages[j].user)
                    break;
                if (i === j)
                    res++;
            }
        return res;
    }

    const sendMessage = useCallback((newMessage, currentUser) => {
        if (newMessage !== "") {
            const timeNow = new Date();
            const message = {
                id: uuidv4(),
                userId: currentUser[0].id,
                avatar: currentUser[0].avatar,
                user: currentUser[0].username,
                text: newMessage,
                createdAt: timeNow,
                editedAt: ""
            }
            setNewMessage("");
            dispatch(chatActionCreator.addMessage(message));
        } else {
            toast.error(`Error: message must not be empty`);
        }
    }, [dispatch]);

    const deleteMessage = useCallback((id) => {
        dispatch(chatActionCreator.deleteMessage(id));
    }, [dispatch]);

    const setStatusIdle = useCallback(() => {
        dispatch(chatActionCreator.setStatusIdle());
    }, [dispatch]);

    const LastMessageFormatter = new Intl.DateTimeFormat("ru", {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit"
    });
    useEffect(() => {
        if(status === 'error') {
            setStatusIdle();
            return toast.error(`Error: ${error}`);
        }
    }, [status, error, setStatusIdle]);

    useEffect(() => {
        if(status === 'success') {
            setStatusIdle();
        }
    }, [status, setStatusIdle]);

    if (!currentUser) {
        return <Redirect to={AppPath.ROOT}></Redirect>
    }

    if (status === "pending") {
        return <Preloader />;
    }

    return (
                <>
                    <Toaster position="top-right" />
                    {preloader ? 
                        <Preloader /> 
                    :
                        <> 
                            <Header 
                                chatTitle="My chat" 
                                userCount={messages ? (messages.length > 0 ? countUsers(messages) : 0) : 0}
                                messageCount={messages ? messages.length : 0}
                                lastMessageDate={messages ? (messages.length > 0 ? LastMessageFormatter.format(new Date(messages[messages.length - 1].createdAt)).replace(/,/g, '') : "none") : "none"}
                            >
                            </Header>
                            <MessageList
                                data={messages}
                                onDataChange={deleteMessage}
                                ownId={currentUser[0].id}
                            >
                            </MessageList>
                            <MessageInput
                                defaultValue={newMessage}
                                onInputChange={(event) => setNewMessage(event.target.value)}
                                sendMessage={() => sendMessage(newMessage, currentUser)}
                                type={"Send"}
                            >
                            </MessageInput>
                            {currentUser ? 
                                (currentUser[0].role === 'admin' ? 
                                    <Link to={AppPath.USER_LIST}>
                                        <button className="button-to-users">
                                            User list
                                        </button>
                                    </Link> 
                                : 
                                    null
                                ) 
                                : 
                                null
                            }
                        </>
                    }
                </>
            );
};

export default Chat;
