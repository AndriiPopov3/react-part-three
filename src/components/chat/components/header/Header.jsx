import './header.css';

const Header = ({ chatTitle, userCount, messageCount, lastMessageDate }) => {
    return (
        <div className="header">
            <div><span className="header-title">{chatTitle}</span></div>
            <div><span className="header-users-count">{userCount}</span> users</div>
            <div><span className="header-messages-count">{messageCount}</span> messages</div>
            <div>Last message: <span className="header-last-message-date">{lastMessageDate}</span></div>
        </div>
    );
}

export default Header;