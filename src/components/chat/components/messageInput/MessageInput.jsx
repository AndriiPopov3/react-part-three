import './messageInput.css';

const MessageInput = ({onInputChange, defaultValue, sendMessage, type}) => {
    return (
        <div className="message-input">
            <input 
                className="message-input-text"
                onChange={onInputChange}
                value={defaultValue}
            >
            </input>
            <button
                className="message-input-button"
                onClick={sendMessage}
            >
                {type}
            </button>
        </div>
    );
}

export default MessageInput;