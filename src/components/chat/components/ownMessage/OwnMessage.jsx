import { Redirect } from 'react-router-dom';
import { AppPath } from 'common/enums/enums';
import { useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { chat as chatActionCreator } from 'store/actions';
import './ownMessage.css';

const OwnMessage = ({ id, text, messageTime, username, userAvatar, onDelete }) => {
    const [isEdit, setIsEdit] = useState(false);
    const dispatch = useDispatch();
    const MessageFormatter = new Intl.DateTimeFormat("ru", {
        hour: "2-digit",
        minute: "2-digit"
    });

    const onEdit = useCallback((id, text) => {
        setIsEdit(true);
        dispatch(chatActionCreator.setEditedMessage({ id, text }));
    }, [dispatch]);

    return (
        <div className="own-message">
            {isEdit ? <Redirect to={AppPath.EDIT_MESSAGE} ></Redirect> : null}
            <div className="message-container">
                <div className="message-text">{text}</div>
                <div className="message-info-container">
                    <div className="message-time">{MessageFormatter.format(new Date(messageTime))}</div>
                    <button
                        className="message-edit"
                        onClick={() => onEdit(id, text)}
                    >
                        Edit
                    </button>
                    <button
                        className="message-delete"
                        onClick={() => onDelete(id)}
                    >
                        Delete
                    </button>
                </div>
            </div>
        </div>
        
    );
}

export default OwnMessage;