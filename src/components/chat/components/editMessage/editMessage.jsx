import { Redirect } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppPath } from 'common/enums/enums';
import { chat as chatActionCreator } from 'store/actions';
import './editMessage.css';

const EditMessage = () => {
    const { editedMessage, editedMessageId } = useSelector(({ chat }) => ({
        editedMessage: chat.editedMessage,
        editedMessageId: chat.editedMessageId
      }));
    const dispatch = useDispatch();
    const [newEditedMessage, setNewEditedMessage] = useState(editedMessage);
    const [editCancel, setEditCancel] = useState(false);
    const onEditCancel = () => {
        // TODO clear values in reducer
        setEditCancel(true);
    }

    const onEditSave = useCallback((editedMessage, editedMessageId) => {
        if(editedMessage !== "") {
            dispatch(chatActionCreator.editMessage({text: editedMessage, id: editedMessageId}));
            setEditCancel(true);
        } else {
            toast.error(`Error: message must not be empty`);
        }
    }, [dispatch]);

    return (
        <div className="edit-message">
            <Toaster position="top-right" />
            <div className="edit-message-container">
                <h3>Edit message</h3>
                <input type="text" 
                       className="edit-message-input" 
                       defaultValue={newEditedMessage} 
                       onChange={(event) => setNewEditedMessage(event.target.value)}
                ></input>
                <button className="edit-message-button" 
                        onClick={() => onEditSave(newEditedMessage, editedMessageId)}
                >
                    Edit
                </button>
                <button className="edit-message-close"
                        onClick={() => onEditCancel()}
                >
                    Close
                </button>
                {editCancel ? <Redirect to={AppPath.CHAT}></Redirect> : null}
            </div>
        </div>
    );
}

export default EditMessage;