import { useEffect, useRef } from "react";
import Message from '../message/Message';
import OwnMessage from '../ownMessage/OwnMessage';
import './messageList.css';

const MessageList = ({ data, onDataChange, ownId }) => {
    const messagesEndRef = useRef(null)
    
    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
    }

    useEffect(() => {
        scrollToBottom()
      }, [data]);

    const DividerFormatter = new Intl.DateTimeFormat("en-AU", {
        weekday: "long",
        day: "numeric",
        month: "long"
    });
    
    const messageList = data?.map(message => {
        return message.userId === ownId ?
            <OwnMessage
                text={message.text}
                messageTime={message.createdAt}
                key={message.id}
                id={message.id}
                onDelete={onDataChange}
            >
            </OwnMessage>
        :
            <Message
                text={message.text}
                messageTime={message.createdAt}
                username={message.user}
                userAvatar={message.avatar}
                key={message.id}
            >
            </Message>
        })

    const messageListDivided = function() {
        let prevMessageDate;
        let messageListDivided = [];
        for (let i = 0; i < messageList.length; i++){
            const datePrev = new Date(prevMessageDate);
            const dateNow = new Date(messageList[i].props.messageTime);
            let newDate = false;
            if (prevMessageDate) {
                if (datePrev.getFullYear() === dateNow.getFullYear() &&
                    datePrev.getMonth() === dateNow.getMonth() &&
                    datePrev.getDate() === dateNow.getDate()) {            
                } else {
                    newDate = true;
                }
            } else {
                const today = new Date();
                if (today.getFullYear() === dateNow.getFullYear() &&
                    today.getMonth() === dateNow.getMonth() &&
                    today.getDate() === dateNow.getDate()) {
                        messageListDivided.push(<div className="messages-divider" key={dateNow}>Today</div>)
                } else {
                    const yesterday = new Date(today);
                    yesterday.setDate(yesterday.getDate() - 1)
                    if (yesterday.getFullYear() === dateNow.getFullYear() &&
                        yesterday.getMonth() === dateNow.getMonth() &&
                        yesterday.getDate() === dateNow.getDate()) {
                            messageListDivided.push(<div className="messages-divider" key={dateNow}>Yesterday</div>)
                } else {
                    messageListDivided.push(<div className="messages-divider" key={dateNow}>{DividerFormatter.format(dateNow)}</div>)
                }
                }
            }
            prevMessageDate = messageList[i].props.messageTime;
            if (newDate) {
                const today = new Date();
                if (today.getFullYear() === dateNow.getFullYear() &&
                    today.getMonth() === dateNow.getMonth() &&
                    today.getDate() === dateNow.getDate()) {
                        messageListDivided.push(<div className="messages-divider" key={dateNow}>Today</div>)
                } else {
                    const yesterday = new Date(today);
                    yesterday.setDate(yesterday.getDate() - 1)
                    if (yesterday.getFullYear() === dateNow.getFullYear() &&
                        yesterday.getMonth() === dateNow.getMonth() &&
                        yesterday.getDate() === dateNow.getDate()) {
                            messageListDivided.push(<div className="messages-divider" key={dateNow}>Yesterday</div>)
                    } else {
                        messageListDivided.push(<div className="messages-divider" key={dateNow}>{DividerFormatter.format(dateNow)}</div>)
                    }
                }
                messageListDivided.push(messageList[i]);
            } else {
                messageListDivided.push(messageList[i]);
            }
        }
        return messageListDivided;
    }

    return (
        <div className="message-list">
            {data ? messageListDivided() : null}
            <div ref={messagesEndRef} />
        </div>
    );
    
}

export default MessageList;
