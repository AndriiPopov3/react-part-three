import { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import './message.css';

const Message = ({ text, messageTime, username, userAvatar }) => {

    const MessageFormatter = new Intl.DateTimeFormat("ru", {
        hour: "2-digit",
        minute: "2-digit"
    });
    const [isLiked, setIsLiked] = useState(false);
    const likeComment = () => {
        setIsLiked(!isLiked);
    }

    return (
        <div className="message">
            <div className="user-info-container">
                <img className="message-user-avatar" src={userAvatar} alt="user-avatar"></img>
                <div className="message-user-name">{username}</div>
            </div>
            <div className="message-text-container">
                <div className="message-text">{text}</div>
                <div className="message-info-container">
                    <div className="message-time">{MessageFormatter.format(new Date(messageTime))}</div>
                        <FontAwesomeIcon
                            icon={faThumbsUp}
                            className={isLiked ? "message-liked" : "message-like"}
                            onClick={() => likeComment()}
                        />
                    </div>
                </div>
            </div>
        );
    
}

export default Message;
