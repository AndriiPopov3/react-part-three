import logo from './logo.svg';
import './preloader.css';

const Preloader = () => {
    return (
        <div className="preloader">
            <img src={logo} className="preloader-logo" alt="logo" />
        </div>
    );
}

export default Preloader;