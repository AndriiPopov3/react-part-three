import React from "react";
import toast, { Toaster } from 'react-hot-toast';
import { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'components/common/common';
import { Redirect } from 'react-router';
import UserItem from './userItem/userItem';
import Preloader from '../chat/components/preloader/Preloader';
import { users as userActionCreator } from 'store/actions';
import { AppPath } from 'common/enums/enums';
import './userList.css';

const UserList = () => {
    const { users, currentUser, preloader, status, error } = useSelector(({ users, login }) => ({
        users: users.users,
		preloader: users.preloader,
		currentUser: login.user,
		status: users.status,
		error: users.error
    }));

	const [isEdit, setIsEdit] = useState(false);

	const dispatch = useDispatch();

	useEffect(() => {
        dispatch(userActionCreator.loadUsers());
    }, [dispatch]);

	const editUser = useCallback((user) => {
		setIsEdit(true);
        dispatch(userActionCreator.setEditedUser(user));
    }, [dispatch]);

	const deleteUser = useCallback((id) => {
        dispatch(userActionCreator.deleteUser(id));
    }, [dispatch]);

	const onAdd = useCallback(() => {
        setIsEdit(true);
        dispatch(userActionCreator.setNewUser());
    }, [dispatch]);

	const setStatusIdle = useCallback(() => {
        dispatch(userActionCreator.setStatusIdle());
    }, [dispatch]);

	useEffect(() => {
        if(status === 'error') {
            setStatusIdle();
            return toast.error(`Error: ${error}`);
        }
    }, [status, error, setStatusIdle]);

	if (!currentUser) {
        return <Redirect to={AppPath.ROOT}></Redirect>
    }

	if (status === "pending") {
        return <Preloader />;
    }

	return (
		<>
		    <Toaster position="top-right" />
			{preloader ? 
				<Preloader /> 
			:
			<>
			{isEdit ? <Redirect to={AppPath.EDIT_USER} ></Redirect> : null}
			<div className="user-block-container">
				<h2 className="user-list-title">User List</h2>
				{
					users.map(user => {
						return (
							<UserItem
								key={user.id}
								user={user}
								onDelete={deleteUser}
								onEdit={editUser}
							/>
						);
					})
				}
				<button className="button-add-user" onClick={() => onAdd()}>Add user</button>
				<Link to={AppPath.CHAT}><button className="button-to-chat">Chat</button></Link>
			</div>
			</>
			}
		</>
	);
}

export default UserList;