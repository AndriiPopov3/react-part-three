import React from "react";
import './userItem.css';

const UserItem = ({ user, onDelete, onEdit }) => {
        return (
            <div className="user-container">
                <img src={user.avatar} className="user-info-avatar" alt="user-avatar"></img>
                <div className="user-info">
                    <div className="user-info-name">{user.username}</div>
                    <div className="user-info-email">{user.email}</div>
                </div>
                <div>
                    <button className="user-button-edit" onClick={() => onEdit(user)}>Edit</button>
                    <button className="user-button-delete" onClick={() => onDelete(user.id)}>Delete</button>
                </div>
            </div>
        );
};

export default UserItem;