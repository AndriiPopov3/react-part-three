import { v4 as uuidv4 } from 'uuid';
import toast, { Toaster } from 'react-hot-toast';
import { Redirect } from 'react-router-dom';
import { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppPath } from 'common/enums/enums';
import { users as userActionCreator } from 'store/actions';
import './editUser.css';

const EditUser = () => {
    const { user } = useSelector(({ users }) => ({
        user: users.newUser
    }));
    
    const dispatch = useDispatch();
    const [newUsername, setUsername] = useState(user?.username);
    const [newPassword, setPassword] = useState(user?.password);
    const [newAvatar, setAvatar] = useState(user?.avatar);
    const [newRole, setRole] = useState(user?.role);
    const [newEmail, setEmail] = useState(user?.email);
    const [editCancel, setEditCancel] = useState(false);
    const [passwordVisible, setPasswordVisible] = useState(false);
    const onEditCancel = () => {
        // TODO clear values in reducer
        setEditCancel(true);
    }

    const onAddSave = useCallback((newUsername, newPassword, newAvatar, newRole, newEmail) => {
        if(newUsername !== "" &&
           newPassword !== "" &&
           newAvatar !== "" &&
           newRole !== "" &&
           newEmail !== "") {
                dispatch(userActionCreator.addUser({
                    id: uuidv4(),
                    username: newUsername, 
                    password: newPassword, 
                    avatar: newAvatar, 
                    role: newRole, 
                    email: newEmail
                }));
                setEditCancel(true);
           } else {
            toast.error(`Error: fields must not be empty`);
           }
    }, [dispatch]);

    const onEditSave = useCallback((newUsername, newPassword, newAvatar, newRole, newEmail) => {
        if(newUsername !== "" &&
           newPassword !== "" &&
           newAvatar !== "" &&
           newRole !== "" &&
           newEmail !== "") {
                dispatch(userActionCreator.editUser({
                    id: user?.id,
                    username: newUsername,
                    password: newPassword,
                    avatar: newAvatar,
                    role: newRole,
                    email: newEmail
                }));
                setEditCancel(true);
        } else {
            toast.error(`Error: fields must not be empty`);
        }
    }, [dispatch, user]);

    return (
        <div className="edit-user">
            <Toaster position="top-right" />
            <div className="edit-user-container">
                <h3>{user?.id !== "" ? "Edit user" : "Add user"}</h3>
                <h4 className="input-title">Username</h4>
                <input type="text" 
                       className="edit-user-input" 
                       defaultValue={newUsername} 
                       onChange={(event) => setUsername(event.target.value)}
                ></input>
                <h4 className="input-title">Password</h4>
                <div className="password-toggler">
                    <input type="checkbox" onClick={() => setPasswordVisible(!passwordVisible)}></input>
                    <span>Show password</span>
                </div>
                <input type={passwordVisible ? "text" : "password"} 
                       className="edit-user-input" 
                       defaultValue={newPassword} 
                       onChange={(event) => setPassword(event.target.value)}
                ></input>
                <h4 className="input-title">Avatar</h4>
                <input type="text" 
                       className="edit-user-input" 
                       defaultValue={newAvatar} 
                       onChange={(event) => setAvatar(event.target.value)}
                ></input>
                <h4 className="input-title">Role</h4>
                <input type="text" 
                       className="edit-user-input" 
                       defaultValue={newRole} 
                       onChange={(event) => setRole(event.target.value)}
                ></input>
                <h4 className="input-title">Email</h4>
                <input type="text" 
                       className="edit-user-input" 
                       defaultValue={newEmail} 
                       onChange={(event) => setEmail(event.target.value)}
                ></input>
                <button className="edit-user-button" 
                        onClick={user?.id !== "" ? () => onEditSave(newUsername, newPassword, newAvatar, newRole, newEmail) : () => onAddSave(newUsername, newPassword, newAvatar, newRole, newEmail)}
                >
                    {user?.id !== "" ? "Edit" : "Add"}
                </button>
                <button className="edit-user-close"
                        onClick={() => onEditCancel()}
                >
                    Close
                </button>
                {editCancel ? <Redirect to={AppPath.USER_LIST}></Redirect> : null}
            </div>
        </div>
    );
}

export default EditUser;