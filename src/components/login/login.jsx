import toast, { Toaster } from 'react-hot-toast';
import { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppPath } from 'common/enums/enums';
import { login as loginActionCreator } from 'store/actions';
import { Redirect } from 'react-router'
import './style.css';

const Login = () => {
    const { user, status, error } = useSelector(({ login }) => ({
        user: login.user,
        status: login.status,
        error: login.error
      }));
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();

    const handleLogin = useCallback((username, password) => {
        dispatch(loginActionCreator.loginUser({username, password}));
      }, [dispatch]);
    
    const loginStatus = status === 'success';
    let userRole;
    if (user) {
        userRole = user[0].role;
    }

    useEffect(() => {
        if(status === 'error') {
            return toast.error(`Error: ${error}`);
        }
    }, [status, error]);

    return  <>
                <Toaster position="top-right" />
                {loginStatus ?
                    (userRole === 'admin' ?
                        <Redirect to={AppPath.USER_LIST}></Redirect>
                    :
                        <Redirect to={AppPath.CHAT}></Redirect>
                    )
                :
                    (<div className="login-container">
                        <div className="login-form">
                            <h3>Username</h3>
                            <input type="text" className="login-form-input" defaultValue={username} onChange={(event) => setUsername(event.target.value)}></input>
                            <h3>Password</h3>
                            <input type="password" className="login-form-input" defaultValue={password} onChange={(event) => setPassword(event.target.value)}></input>
                            <button className="login-button" onClick={() => handleLogin(username, password)}>Login</button>
                        </div>
                    </div>
                    )
                }
            </>
};

export default Login;
