import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadUsers = createAsyncThunk(ActionType.LOAD_USERS, async (user, { extra }) => {
    const users = await extra.userService.getAll();
      return {
      users,
    };
  });

const deleteUser = createAsyncThunk(ActionType.DELETE_USER, async (id, { extra }) => {
    await extra.userService.deleteUser(id);
    
    return {
      id,
    };
  });

const setNewUser = createAction(ActionType.SET_NEW_USER, () => ({
    payload: {}
  }));

  const setEditedUser = createAction(ActionType.SET_EDITED_USER, (user) => ({
    payload: user
  }));

  const addUser = createAsyncThunk(ActionType.ADD_USER, async (user, { extra }) => ({
    newUser: await extra.userService.add(user),
  }));

  const editUser = createAsyncThunk(ActionType.EDIT_USER, async ({ id, username, password, avatar, role, email }, { extra }) => ({
    newUser: await extra.userService.edit(id, { username, password, avatar, role, email }),
  }));

  const setStatusIdle = createAction(ActionType.SET_STATUS_IDLE, () => ({}));

  export {
    loadUsers,
    deleteUser,
    setNewUser,
    addUser,
    setEditedUser,
    editUser,
    setStatusIdle
  };