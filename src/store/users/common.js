const ActionType = {
    LOAD_USERS: 'users/load-users',
    DELETE_USER: 'users/delete-user',
    SET_NEW_USER: 'users/set-new-user',
    SET_EDITED_USER: 'users/set-edited-user',
    ADD_USER: 'users/add-user',
    EDIT_USER: 'users/edit-user',
    SET_STATUS_IDLE: 'users/set-status-idle'
};
  
export { ActionType };
 