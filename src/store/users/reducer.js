import { createReducer } from '@reduxjs/toolkit';
import { DataStatus } from 'common/enums/enums';
import { loadUsers,
         deleteUser,
         setNewUser,
         setEditedUser,
         addUser,
         editUser,
         setStatusIdle
       } from './actions';

const initialState = {
    users: [],
    status: DataStatus.IDLE,
    preloader: true,
    newUser: null,
    error: null
};

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(loadUsers.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(loadUsers.fulfilled, (state, { payload }) => {
      const { users } = payload;
        state.users = users;
        state.status = DataStatus.IDLE;
        state.preloader = false;
    });
    builder.addCase(loadUsers.rejected, (state) => {
      state.status = DataStatus.ERROR;
      state.error = "server error";
    });
    builder.addCase(deleteUser.rejected, (state) => {
      state.status = DataStatus.ERROR;
      state.error = "unable to delete user";
    });
    builder.addCase(deleteUser.pending, (state) => {
      state.status = DataStatus.PENDING;
    });
    builder.addCase(deleteUser.fulfilled, (state, { payload }) => {
      const { id } = payload;
      state.status = DataStatus.SUCCESS;
      state.users = state.users.filter((user) => user.id !== id);
    });
    builder.addCase(setNewUser, (state, {payload}) => {
      const newUser = {
        id: "",
        avatar: "",
        username: "",
        password: "",
        role: "",
        email: ""
      }
      state.newUser = newUser;
    });
    builder.addCase(setEditedUser, (state, {payload}) => {
      const editedUser = {
        id: payload.id,
        avatar: payload.avatar,
        username: payload.username,
        password: payload.password,
        role: payload.role,
        email: payload.email
      }
      state.newUser = editedUser;
    });
    builder.addCase(addUser.fulfilled, (state, { payload }) => {
      const { newUser } = payload;
      state.users = state.users.concat(newUser);
    });
    builder.addCase(addUser.rejected, (state) => {
      state.status = DataStatus.ERROR;
      state.error = "unable to add user";
    });
    builder.addCase(editUser.fulfilled, (state, {payload}) => {
      const { newUser } = payload;
      state.users = state.users.map((user) => {
        return user.id === newUser.id ? { ...user, ...newUser } : user;
      });
    });
    builder.addCase(editUser.rejected, (state) => {
      state.status = DataStatus.ERROR;
      state.error = "unable to edit user";
    });
    builder.addCase(setStatusIdle, (state) => {
      state.status = DataStatus.IDLE;
    });
});

export { reducer };
