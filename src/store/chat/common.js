const ActionType = {
    LOAD_DATA: 'messages/load-messages',
    ADD_MESSAGE: 'messages/add-message',
    DELETE_MESSAGE: 'messages/delete-message',
    SET_EDITED_MESSAGE: 'messages/set-edited-message',
    EDIT_MESSAGE: 'messages/edit-message',
    SET_STATUS_IDLE: 'messages/set-status-idle'
  };
  
  export { ActionType };
  