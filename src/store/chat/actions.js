import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadMessages = createAsyncThunk(ActionType.LOAD_DATA, async (user, { extra }) => {
    const messages = await extra.messageService.getAll();
      return {
      messages,
    };
  });

const addMessage = createAsyncThunk(ActionType.ADD_MESSAGE, async (message, { extra }) => ({
  newMessage: await extra.messageService.add(message),
}));

const deleteMessage = createAsyncThunk(ActionType.DELETE_MESSAGE, async (id, { extra }) => {
  await extra.messageService.deleteMessage(id);
  
  return {
    id,
  };
});

const editMessage = createAsyncThunk(ActionType.EDIT_MESSAGE, async ({ id, text }, { extra }) => ({
  newMessage: await extra.messageService.edit(id, { text }),
}));

const setEditedMessage = createAction(ActionType.SET_EDITED_MESSAGE, (message_data) => ({
  payload: {
    message_data
  }
}));

const setStatusIdle = createAction(ActionType.SET_STATUS_IDLE, () => ({}));

  export {
    loadMessages,
    addMessage,
    deleteMessage,
    editMessage,
    setEditedMessage,
    setStatusIdle
  };