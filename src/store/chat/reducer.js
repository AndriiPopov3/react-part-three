import { createReducer } from '@reduxjs/toolkit';
import { DataStatus } from 'common/enums/enums';
import { loadMessages,
         addMessage,
         deleteMessage,
         setEditedMessage,
         editMessage,
         setStatusIdle
       } from './actions';

const initialState = {
    messages: [],
    preloader: true,
    newMessage: "",
    status: DataStatus.IDLE,
    editedMessage: null,
    editedMessageId: null,
    error: null
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(loadMessages.pending, (state) => {
    state.status = DataStatus.PENDING;
    state.preloader = true;
  });
  builder.addCase(loadMessages.fulfilled, (state, { payload }) => {
    const { messages } = payload;
      state.messages = messages;
      state.status = DataStatus.SUCCESS;
      state.preloader = false;
  });
  builder.addCase(loadMessages.rejected, (state) => {
    state.status = DataStatus.ERROR;
    state.error = "server error";
  });
  builder.addCase(addMessage.fulfilled, (state, { payload }) => {
    const { newMessage } = payload;
    state.status = DataStatus.SUCCESS;
    state.messages = state.messages.concat(newMessage);
  });
  builder.addCase(addMessage.pending, (state) => {
    state.status = DataStatus.PENDING;
  });
  builder.addCase(addMessage.rejected, (state) => {
    state.status = DataStatus.ERROR;
    state.error = "failed to send message";
  });
  builder.addCase(deleteMessage.fulfilled, (state, { payload }) => {
    const { id } = payload;
    state.status = DataStatus.SUCCESS;
    state.messages = state.messages.filter((message) => message.id !== id);
  });
  builder.addCase(deleteMessage.pending, (state) => {
    state.status = DataStatus.PENDING;
  });
  builder.addCase(deleteMessage.rejected, (state) => {
    state.status = DataStatus.ERROR;
    state.error = "failed to delete message";
  });
  builder.addCase(setEditedMessage, (state, {payload}) => {
    const { message_data } = payload;
    state.editedMessage = message_data.text;
    state.editedMessageId = message_data.id;
  });
  builder.addCase(setStatusIdle, (state) => {
    state.status = DataStatus.IDLE;
  });
  builder.addCase(editMessage.fulfilled, (state, {payload}) => {
    const { newMessage } = payload;
    state.status = DataStatus.SUCCESS;
    state.messages = state.messages.map((message) => {
      return message.id === newMessage.id ? { ...message, ...newMessage } : message;
    });
  });
  builder.addCase(editMessage.rejected, (state) => {
    state.status = DataStatus.ERROR;
    state.error = "failed to edit message";
  });
  builder.addCase(editMessage.pending, (state) => {
    state.status = DataStatus.PENDING;
  });
});

export { reducer };
