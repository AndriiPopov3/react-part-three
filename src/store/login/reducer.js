import { createReducer } from '@reduxjs/toolkit';
import { DataStatus } from 'common/enums/enums';
import { loginUser } from './actions';

const initialState = {
  user: null,
  status: DataStatus.IDLE,
  error: null
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(loginUser.pending, (state) => {
    state.status = DataStatus.PENDING;
  });
  builder.addCase(loginUser.rejected, (state) => {
    state.status = DataStatus.ERROR;
    state.error = "server error";
  });
  builder.addCase(loginUser.fulfilled, (state, { payload }) => {
    const { userFound } = payload;
    if(userFound.length > 0) {
      state.user = userFound;
      state.status = DataStatus.SUCCESS;
    } else {
      state.status = DataStatus.ERROR;
      state.error = "user not found";
    }
  });
});

export { reducer };
