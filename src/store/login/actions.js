import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loginUser = createAsyncThunk(ActionType.LOGIN_USER, async (user, { extra }) => {
  const userFound = await extra.userService.getOne(user);
    return {
      userFound,
    };
  });

  export {
    loginUser
  };