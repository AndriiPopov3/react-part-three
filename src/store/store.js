import { configureStore } from '@reduxjs/toolkit';
import { user as userService, message as messageService } from 'services/services';
import { login, chat, users } from './root-reducer';

const store = configureStore({
  reducer: {
    login,
    chat,
    users
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      thunk: {
        extraArgument: {
          userService,
          messageService
        },
      },
      serializableCheck: false,
    });
  },
});

export { store };
