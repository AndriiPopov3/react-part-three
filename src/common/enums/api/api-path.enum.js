const ApiPath = {
  TODOS: '/todos',
  USERS: '/users',
  MESSAGES: '/messages'
};

export { ApiPath };
