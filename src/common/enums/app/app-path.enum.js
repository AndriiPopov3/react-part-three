const AppPath = {
  ROOT: '/',
  CHAT: '/chat',
  EDIT_MESSAGE: '/edit-message',
  EDIT_USER: '/edit-user',
  USER_LIST: '/user-list',
  ANY: '*',
};

export { AppPath };
